#include "list.h"


ObjList::~ObjList()
{
  struct l_list* tmp;
 
  list_ptr = data;

  // free all the memory we've allocated.
  while(list_ptr != NULL){
    tmp = list_ptr->next;
    if(list_ptr->obj != NULL)
      delete list_ptr->obj;
    delete list_ptr;
    list_ptr = tmp;
  }
}

/*const*/ BaseObject* ObjList::get_object(void)
{
  if(list_ptr != NULL)
    return this->list_ptr->obj;
  return NULL;
}

void ObjList::goto_end(void)
{
  while(list_ptr->next != NULL)
    list_ptr = list_ptr->next;
}


void ObjList::goto_start(void)
{
  list_ptr = data;
}

// Remove the current item in list.
// After removal, the internal list ptr will point
// to the next object (which could be NULL ofcourse)
void ObjList::delete_obj(void)
{
  if(list_ptr != NULL){
    
    // is the current item the first one?
    if(data == list_ptr){
      // first and only one?
      if(data->next == NULL){
	delete data->obj;
	delete data;
	data = NULL;
	list_ptr = data;
      }
      // first, but not the only one
      else{
	data = data->next;
	data->prev = NULL;
	delete list_ptr->obj;
	delete list_ptr;
	list_ptr = data;
      }
    }

    // Not the first object
    else{
      struct l_list *tmp;

      tmp = list_ptr->prev;
      tmp->next = list_ptr->next;
      delete list_ptr->obj;
      delete list_ptr;
      list_ptr = tmp->next;

      // If there's a 'next item' in the list, we need to update
      // its prev pointer so that it doesn't point to the item
      // we just deleted.
      if(list_ptr != NULL)
	list_ptr->prev = tmp;      

    }
    size--;
  }
}


void ObjList::add_obj(BaseObject *new_obj)
{
  struct l_list *tmp = data;

  if(data == NULL){
    tmp = new struct l_list;
    tmp->next = NULL;
    tmp->prev = NULL;
    tmp->obj = new_obj;
    data = tmp;
    list_ptr = data;
    size = 1;
  }
  else{
    // find last item in list.
    while(tmp->next != NULL)
      tmp = tmp->next;

    tmp->next = new struct l_list;

    tmp->next->prev = tmp;
    tmp->next->next = NULL;
    tmp->next->obj = new_obj;

    size++;
  }
}

// Go to the next object, return true if it exists.
// Else, do nothing and return false.
bool ObjList::next_obj(void)
{
  list_ptr = list_ptr->next;
  if(list_ptr != NULL)
    return true;

  return false;
}


// If there is a 'previous' object, go to it and return true.
// Else, do nothing and return false.
bool ObjList::prev_obj(void)
{
  list_ptr = list_ptr->prev;
  if(list_ptr != NULL)
    return true;

  return false;
}


// True - yes, we're at the last object in the list
// False - nope, we're somewhere else
bool ObjList::at_end(void)
{
  if(list_ptr->next == NULL)
    return true;

  return false;
}


// True - yes, we're at the first object in the list
// False - nope, we're somewhere else
bool ObjList::at_start(void)
{
  if(list_ptr->prev == NULL)
    return true;

  return false;
}


unsigned int ObjList::get_size(void)
{
  return size;
}

// Returns:
//    - NULL if not found
//    - obj  if found.
const BaseObject* ObjList::find_object(unsigned int idx)
{
  struct l_list *tmp = data;

  while((tmp != NULL) && (idx > 0)){
    tmp = tmp->next;
    idx--;
  }

   //  for(struct l_list *tmp = data; (tmp != NULL) && (idx > 0); idx--)
   // tmp=tmp->next;

  return tmp->obj;
}

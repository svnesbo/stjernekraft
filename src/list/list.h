#ifndef _LLIST
#define _LLIST

#include "../baseobj.h"


struct l_list {
  BaseObject *obj;
  struct l_list *prev;
  struct l_list *next;
};

// Legg til: liste med hver type objekt?


class ObjList {
public:
  ObjList() {data = 0; list_ptr = 0; size = 0; }
  ~ObjList();
  /*const*/ BaseObject *get_object(void); // get current object.
  void goto_end(void);
  void goto_start(void);
  void delete_obj(void);
  void add_obj(BaseObject *new_obj);
  bool next_obj(void);
  bool prev_obj(void);
  bool at_end(void);
  bool at_start(void);
  unsigned int get_size(void);
  const BaseObject *find_object(unsigned int idx);

  // friend collision_detector(...) ?
 private:
  struct l_list *data, *list_ptr;
  unsigned int size;
};


#endif

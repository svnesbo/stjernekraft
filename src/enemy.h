#ifndef _ENEMY_H
#define _ENEMY_H

#include "spaceship.h"
#include "projectile.h"


// AI types:
//
// 1 - I fly straight downwards, and shoot occasionally
// 2 - I am a suicide bomber
enum _enemy_ai_t {STRAIGHT_DOWNWARDS, SUICIDE_BOMBER};


class Enemy : public Spaceship
{
 public:
  Enemy(int x, int y, int speed_x, int speed_y,
	int speed_x_max, int speed_y_max, int acc_x, int acc_y, 
	unsigned int hp, unsigned int hp_max, unsigned int armor,
	unsigned int armor_max, _status_t status, _enemy_ai_t AI_type,
	unsigned int fire_rate, int score_value);   
  
  // Some of our enemies (such as the suicide bombers!) will probably need
  // access to the players coords etc.
  // void do_AI_stuff(Player the_good_guy, unsigned int time_elapsed);
  Projectile *do_AI_stuff(unsigned int time_elapsed);
  int get_score_value(void) { return score_value; }
 private:
  Projectile* fire_laser(void);
  _enemy_ai_t AI_type;
  // milliseconds per shot
  unsigned int fire_rate;
  unsigned int reload_timer;
  int score_value;
};




#endif

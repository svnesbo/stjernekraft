#ifndef _MENU_H
#define _MENU_H

#include "altera_up_avalon_pixel_buffer.h"
#include "altera_up_avalon_ps2.h"

// Number of menu entries.
const unsigned int MENU_MAX = 2;

// Menu entries.
const unsigned int MENU_START_GAME = 0;
const unsigned int MENU_HIGHSCORES = 1;
const unsigned int MENU_INTRO = 2;

// Position of menu "sprite"
const unsigned int MENU_X = 0;
const unsigned int MENU_Y = 160;


unsigned int menu(alt_up_pixel_buffer_dev *pixel_buffer,  alt_up_ps2_dev *ps2_dev);
void update_menu(alt_up_pixel_buffer_dev *pixel_buffer, unsigned int choice);


#endif

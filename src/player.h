#ifndef _PLAYER_H
#define _PLAYER_H

#include "spaceship.h"

class Player : public Spaceship
{
 public:
  Player(int x, int y, int speed_x, int speed_y,
	 int speed_x_max, int speed_y_max, int acc_x, int acc_y, 
	 unsigned int hp, unsigned int hp_max, unsigned int armor,
	 unsigned int armor_max, _status_t status);

};


#endif

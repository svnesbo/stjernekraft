#ifndef _BRIEF_H
#define _BRIEF_H

#include "altera_up_avalon_pixel_buffer.h"
#include "altera_up_avalon_character_buffer.h"
#include "altera_up_avalon_ps2.h"

void brief(alt_up_pixel_buffer_dev *pixel_buffer, 
	   alt_up_char_buffer_dev *char_buffer,
	   alt_up_ps2_dev *ps2_dev, unsigned int lvl);

#endif

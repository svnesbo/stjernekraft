#include "collision.h"


// Go through ObjList's of ships and projectiles,
// checking for collisions.
void collision_detector(ObjList *fire, ObjList *ships)
{
  Projectile *proj_ptr;
  Spaceship *ship_ptr;

  // Return if one of the lists are empty.
  if((fire->get_size() == 0 ) || (ships->get_size() == 0))
    return;

  fire->goto_start();
  proj_ptr = (Projectile*)fire->get_object();
  
  // Go through all projectiles in list
  while(proj_ptr != NULL){
    ships->goto_start();
    ship_ptr = (Spaceship*)ships->get_object();

    // Go through all ships in list.
    while(ship_ptr != NULL){
      // Check for collision.
      // Damage is also calculated by the subsequent
      // collision_detector() functions.
      collision_detector(proj_ptr, ship_ptr);
      ships->next_obj();
      ship_ptr = (Spaceship*)ships->get_object();
    }

    fire->next_obj();
    proj_ptr = (Projectile*)fire->get_object();
  }
}


void collision_detector(ObjList *fire, Spaceship *ship)
{
  Projectile *proj_ptr;

  // Return if list with projectilles/fire is empty,
  // or if the ship pointer is null.
  if((fire->get_size() == 0) || (ship == NULL))
    return;

  fire->goto_start();
  proj_ptr = (Projectile*)fire->get_object();

  while(proj_ptr != NULL){
    collision_detector(proj_ptr, ship);
    fire->next_obj();
    proj_ptr = (Projectile*)fire->get_object();
  }
}


void collision_detector(Projectile *proj, Spaceship *ship)
{
  if(collision_detector((BaseObject*)proj, (BaseObject*)ship) == true){
    proj->hit();
    ship->damage(proj->get_damage());
  }
}


void collision_ship_ships(Spaceship *ship, ObjList *ships)
{
  Spaceship *ships_ptr;

  // Return if list with projectilles/fire is empty,
  // or if the ship pointer is null.
  if((ships->get_size() == 0) || (ship == NULL))
    return;

  ships->goto_start();
  ships_ptr = (Spaceship*)ships->get_object();

  while(ships_ptr != NULL){
    if(collision_detector((BaseObject*)ships_ptr, (BaseObject*)ship)){
      unsigned int ship1_hp, ship2_hp;

      // Calculate total hp and armor both ships currently have
      ship1_hp = ship->get_hp() + ship->get_armor();
      ship2_hp = ships_ptr->get_hp() + ships_ptr->get_armor();

      // Both ships take damage equal the amount of hp/armor the other
      // ship has.
      ship->damage(ship2_hp);
      ships_ptr->damage(ship1_hp);
    }
    ships->next_obj();
    ships_ptr = (Spaceship*)ships->get_object();
  }
}

bool collision_detector(BaseObject *obj1, BaseObject *obj2)
{
  int x1_min, x1_max, x2_min, x2_max;
  int y1_min, y1_max, y2_min, y2_max;

  x1_min = obj1->get_x();
  x1_max = x1_min + obj1->get_sprite_x();

  x2_min = obj2->get_x();
  x2_max = x2_min + obj2->get_sprite_x();

  y1_min = obj1->get_y();
  y1_max = y1_min + obj1->get_sprite_y();
  
  y2_min = obj2->get_y();
  y2_max = y2_min + obj2->get_sprite_y();


  if(((x1_min >= x2_min) && (x1_min <= x2_max)) ||
     ((x1_max >= x2_min) && (x1_max <= x2_max)))
    if(((y1_min >= y2_min) && (y1_min <= y2_max)) ||
       ((y1_max >= y2_min) && (y1_max <= y2_max)))
      return true;

  return false;
}

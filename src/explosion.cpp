#include "explosion.h"


Explosion::Explosion(int x, int y, unsigned int sprite_x, 
		     unsigned int sprite_y, unsigned short *animations,
		     unsigned int n_frames, unsigned int time_per_frame)
  // explosions only have x/y coords, no speed etc.
  : BaseObject(x, y, 0, 0, 0, 0, 0, 0)
{
  this->sprite_x = sprite_x;
  this->sprite_y = sprite_y;
  this->animations = animations;
  this->n_frames = n_frames;
  this->time_per_frame = time_per_frame;
  this->time = 0;
  this->status = NOT_DONE_YET;
}


void Explosion::update_animation(unsigned int time_elapsed)
{
  time += time_elapsed;

  unsigned int frame = time / time_per_frame;

  // Ensure that we don't go past the last animation, 
  // exceeding the memory area for the animations.
  // Also update status, so this object can be deleted sometime.
  if(frame > (n_frames - 1)){
    frame = n_frames - 1;
    status = DONE;
  }

  this->sprite = animations + (frame * sprite_x * sprite_y);
}

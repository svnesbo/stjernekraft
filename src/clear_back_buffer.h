#ifndef _CLR_BACK_BUFFER_H
#define _CLR_BACK_BUFFER_H

#include "altera_up_avalon_pixel_buffer.h"


void clear_back_buffer(alt_up_pixel_buffer_dev *pixel_buffer);


#endif

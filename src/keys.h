#ifndef _KEYS_H
#define _KEYS_H

#include <alt_types.h>
#include "altera_up_ps2_keyboard.h"
#include "altera_up_avalon_ps2_regs.h"


// Key map codes.
// SW[7..0] are shifted left 4 bits.
const unsigned int KEY_RIGHT = 0x0001;
const unsigned int KEY_DOWN = 0x0002;
const unsigned int KEY_UP = 0x0004;
const unsigned int KEY_LEFT = 0x0008;

const unsigned int KEY_FIRE1 = 0x0010;
const unsigned int KEY_FIRE2 = 0x0020;
const unsigned int KEY_SW2 = 0x0040;
const unsigned int KEY_SW3 = 0x0080;
const unsigned int KEY_SW4 = 0x0100;
const unsigned int KEY_SW5 = 0x0200;
const unsigned int KEY_SW6 = 0x0400;
const unsigned int KEY_SW7 = 0x0800;

// Some keyboard make codes
const unsigned int KB_SPACEBAR = 0x0029;
const unsigned int KB_L_CTRL = 0x0014;
const unsigned int KB_LEFT = 0x006B;
const unsigned int KB_RIGHT = 0x0074;
const unsigned int KB_UP = 0x0075;
const unsigned int KB_DOWN = 0x0072;


// When using keyboard, CALL THIS BEFORE get_keys(),
// and allow for a little delay before checking keys.
// This is because of buggy altera ps/2 implementation.
//
// true - keys available, call get_keys()
//        (if no ps2_dev, will always return true).
// false - no keys available yet.
bool key_check(alt_up_ps2_dev *ps2_dev);

// If ps2_dev points to a valid PS/2 device, use keyboard.
// Else use impulse keys.
unsigned int get_keys(alt_up_ps2_dev *ps2_dev, bool keys_ready);

#endif

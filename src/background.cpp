#include "background.h"
#include "system.h"
#include "altera_up_avalon_pixel_buffer.h"
#include <io.h>

// Draw a background pic (16-bit RGB, in an array) to the pixel buffer.
// Assumes 320x240 resolution.
// bg: pointer to background-pic array.
void draw_background(alt_up_pixel_buffer_dev *pixel_buffer, unsigned int *bg)
{
  register unsigned int addr;
  register unsigned int offset = 0;
  register unsigned int limit;

  addr = pixel_buffer->back_buffer_start_address;

  limit = 320*240*2;

  while(offset < limit){
    IOWR_32DIRECT(addr, offset, bg[offset >> 2]);
    offset += 4;
  }
}


// Draw a background pic (16-bit RGB, in an array) to the pixel buffer, 
// with scroll.
// Assumes 320x240 resolution.
// bg: pointer to background-pic array.
// bg_length: length/y/height of background picture
// line_offset: how many lines (y) you want it scrolled (upwards).
void draw_scrolling_background(alt_up_pixel_buffer_dev *pixel_buffer, unsigned int *bg, unsigned int bg_length, unsigned int line_offset)
{
  register unsigned int addr;
  register unsigned int offset = 0;
  register unsigned int bg_offset;
  unsigned int bg_offset2 = 0;
  register unsigned int limit1, limit2;

  addr = pixel_buffer->back_buffer_start_address;

  // Check how far into the background picture we are.
  // Can we just copy straight over, or are we so far into the
  // bg-pic that have to take one part from the bottom, and the
  // rest from the top?
  if((line_offset + 240) > bg_length){
    bg_offset = ((2*bg_length) - (line_offset + 240)) * ((320*2)/4);
    limit1 = ((line_offset + 240) - bg_length) * 320 * 2;
    limit2 = 320*240*2;
  }

  // We can just copy straight from bg_offset. yay.
  else{
    bg_offset = (bg_length - 240 - line_offset)*320/2;
    limit1 = 320*240*2;
    limit2 = 0;
  }




  while(offset < limit1){
    IOWR_32DIRECT(addr, offset, bg[bg_offset]);
    offset += 4;
    bg_offset++;
  }


  // Copy the remanining part of bg-pic here, 
  // if we couldn't do it all in one go.
  bg_offset = 0;
  while(offset < limit2){
    IOWR_32DIRECT(addr, offset, bg[bg_offset]);
    offset += 4;
    bg_offset++;
  }

}


// Draw a background pic (16-bit RGB, in an array) to the pixel buffer.
// Assumes 320x240 resolution.
// bg: pointer to background-pic array.
// line_offset: how many lines (y) you want it scrolled (upwards).
void draw_scrolling_background(alt_up_pixel_buffer_dev *pixel_buffer, unsigned int *bg, unsigned int line_offset)
{
  register unsigned int addr;
  register unsigned int offset = 0;
  register unsigned int bg_offset;
  register unsigned int limit1, limit2;

  addr = pixel_buffer->back_buffer_start_address;


  limit1 = 320*240*2;
  limit2 = 320*line_offset*2;
  offset = limit2;


  bg_offset = 0;



  while(offset < limit1){
    IOWR_32DIRECT(addr, offset, bg[bg_offset]);
    offset += 4;
    bg_offset++;
  }

  offset = 0;

  while(offset < limit2){
    IOWR_32DIRECT(addr, offset, bg[bg_offset]);
    offset += 4;
    bg_offset++;
  }

}

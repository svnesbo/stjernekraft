#include "game.h"
#include "dimensions.h"
#include "player.h"
#include "enemy.h"
#include "projectile.h"
#include "brief.h"
#include "keys.h"
#include "background.h"
#include "v_bar.h"
#include "explosion.h"
#include "collision.h"
#include "sprite.h"
#include "data/data.h"
#include "timer.h"
#include <stdlib.h>
#include <stdio.h>


static inline void draw_objects(alt_up_pixel_buffer_dev *pixel_buffer, ObjList &objects);


void game_start(alt_up_pixel_buffer_dev *pixel_buffer, 
		alt_up_char_buffer_dev *char_buffer,
		alt_up_ps2_dev *ps2_dev)
{
  unsigned int game_over = 0;
  unsigned int lvl_count;
  int score_total = 0;

  // Main loop
  for(lvl_count = 0; !game_over && lvl_count < MAX_LVL; lvl_count++){
    brief(pixel_buffer, char_buffer, ps2_dev, lvl_count);
    if(game(pixel_buffer, char_buffer, ps2_dev, data_levels[lvl_count], &score_total) == false)
      return;
  }

  // IMPLEMENT HIGHSCORES!
}

// Return: true: continue game
//         false: game over.
//
// start_score argument: accumulated score is added to this variable.
bool game(alt_up_pixel_buffer_dev *pixel_buffer, 
	  alt_up_char_buffer_dev *char_buffer, 
	  alt_up_ps2_dev *ps2_dev, level_data lvl, int *score)
{
  ObjList enemies;
  ObjList friendly_fire;
  ObjList enemy_fire;
  ObjList explosions;
  Player p1(150, 180, 0, 0, 4, 4, 1, 1, 500, 500, 500, 500, OKAY);
  p1.set_sprite(bc_icon, BATTLECRUISER_X, BATTLECRUISER_Y);


  // MOVE TO PLAYER/SPACESHIP CLASS!
  int wpn1_reload = 500;
  int wpn2_reload = 1000;


  Enemy* enemy_ptr = NULL;
  Projectile* proj_ptr = NULL;
  Explosion* explosion_ptr = NULL;

  unsigned int time, time_ms, t0, t1;

  unsigned int bg_scroll = 0;

  // keys pressed
  unsigned int k = 0;
  
  // keyboard fifo ready to be read?
  bool keys_ready = false;
  
  // Number of enemies kiled and that got away 
  // Not really used for anything atm.
  unsigned int enemies_escaped = 0;
  unsigned int enemies_killed = 0;

  int score_counter = *score;


  // Flag used for freezing score when game is over.
  int game_over = 0;

  // timer is in milliseconds
  int spawn_timer = 5000;

  // Vars to hold randomly generated x, y, y speed, fire rate
  // for enemies that are spawned.
  unsigned int rnum, r_x, r_sy, r_fr;



  time = TIMER_READ;
  // Use time from program start till this point as random seed.
  // The seed will always be different, as the time depends on 
  // how long it takes the user to start the game.
  srand(time);

  TIMER_RESET;
  t0 = 0;

  alt_up_char_buffer_clear(char_buffer);
  
  // "2nd" main loop, or ingame-loop rather.
  while(1){
    t1 = TIMER_READ;
    TIMER_RESET;

    // Time elapsed last loop-cycle.
    time_ms = (t1 - t0)/50000;

    TIMER_START;
    t0 = TIMER_READ;


    wpn1_reload -= time_ms;
    if(wpn1_reload < 0)
      wpn1_reload = 0;
    
    wpn2_reload -= time_ms;
    if(wpn2_reload < 0)
      wpn2_reload = 0;


    // wait for buffer swap to complete
    while(alt_up_pixel_buffer_check_swap_buffers_status(pixel_buffer) != NULL)
      ;
    
    // draw and scroll down background
    draw_scrolling_background(pixel_buffer, (unsigned int*) lvl.background, 
			      lvl.bg_y, bg_scroll);
    bg_scroll += lvl.bg_scroll_speed;
    if(bg_scroll >= lvl.bg_y)
      bg_scroll = 0;

    // draw all objects
    draw_objects(pixel_buffer, friendly_fire);
    draw_objects(pixel_buffer, enemy_fire);
    draw_objects(pixel_buffer, enemies);
    p1.draw_obj(pixel_buffer);
    draw_objects(pixel_buffer, explosions);  
    
    // Stop updating hp/armor/score if game is over.
    if(p1.get_status() != DEAD)
      draw_armor_hp_score(pixel_buffer, char_buffer, score_counter, &p1);

    alt_up_pixel_buffer_swap_buffers(pixel_buffer);

    // ---------------------------
    //   GAME OVER SCREEN
    // ---------------------------
    if(p1.get_status() == DEAD){
      if(game_over == 0){
	// save current score
	*score = score_counter;
	game_over = 1;
      }
      // Display game over screen.
      // The game_over_screen() function returns false after displaying
      // screen for a few secs.
      if(game_over_screen(char_buffer, *score, time_ms) == true)
	return false;  // false = GAME OVER
    }


    // ---------------------------
    //      Player movement
    // ---------------------------
    k = get_keys(ps2_dev, keys_ready);

    // This check is done after get_keys, to allow for a little delay
    // because of multi-byte make codes needs to be fully read before
    // reading them.
    keys_ready = key_check(ps2_dev);

    // You're not allowed to move if you're not alive!
    if(p1.get_status() != DEAD){
      if(k & KEY_RIGHT){
	// If ship has speed in left direction and is at x = 0,
	// we allow it to immediately start moving.
	// Having to deaccelerate when it's standing still at position 0
	// would be kind of awkward.
	if((p1.get_x() == 0) && p1.get_speed_x() < 0)
	  p1.zero_speed_x();
	// If ship is headed the opposite direction, increase
	// speed twice as much to turn it. Makes everything feel much smoother
	if(p1.get_speed_x() < 0)
	  p1.inc_speed_x(4);
	p1.inc_speed_x(4);
      }
      if(k & KEY_LEFT){
	// If ship has speed in right direction and is at x = xmax,
	// we allow it to immediately start moving.
	if((p1.get_x() == (X_MAX - p1.get_sprite_x() - 1))
	   && p1.get_speed_x() > 0)
	  p1.zero_speed_x();
	// If ship is headed the opposite direction, increase
	// speed twice as much to turn it. Makes everything feel much smoother
	if(p1.get_speed_x() > 0)
	  p1.inc_speed_x(-4);
	p1.inc_speed_x(-4);
      }
      if(k & KEY_UP){
	// If ship has speed downwards and is at y = ymax,
	// we allow it to immediately start moving.
	if((p1.get_y() == Y_MAX - p1.get_sprite_y() - 1)
	   && p1.get_speed_y() > 0)
	  p1.zero_speed_y();
	// If ship is headed the opposite direction, increase
	// speed twice as much to turn it. Makes everything feel much smoother
	if(p1.get_speed_y() > 0)
	  p1.inc_speed_y(-4);
	p1.inc_speed_y(-4);
      }
      if(k & KEY_DOWN){
	// If ship has speed upwards and is at y = 0,
	// we allow it to immediately start moving.
	if(p1.get_y() == 0 && p1.get_speed_y() < 0)
	  p1.zero_speed_y();
	// If ship is headed the opposite direction, increase
	// speed twice as much to turn it. Makes everything feel much smoother
	if(p1.get_speed_y() < 0)
	  p1.inc_speed_y(4);
	p1.inc_speed_y(4);
      }
      if((k & KEY_FIRE1) && wpn1_reload == 0){
	// Laser width: 20
	unsigned int x = p1.get_x() + ((p1.get_sprite_x() - LASER1_X) / 2);
	unsigned int y = p1.get_y();

	// randomly place the lasers +-3 pixels from the middle of the ship
	x += ((rand() % 7) - 3);

	proj_ptr = new Projectile(x, y, 0, -150, 0, -150, 0, 0,
				  Projectile::STRAIGHT_UPWARDS, 50, 
				  Projectile::IN_TRANSIT);

	proj_ptr->set_sprite(laser1, LASER1_X, LASER1_Y);
	friendly_fire.add_obj(proj_ptr);

	wpn1_reload = 150;
      }
      if((k & KEY_FIRE2) && wpn2_reload == 0){
	// Fire missiles!
	wpn2_reload = 1000;
      }
    }



    // MOVE TO BASEOBJ CLASS!
    // gradually decrease speed if the user isn't pressing any keys,
    // and check x/y boundaries.
    if(!(k & (KEY_LEFT | KEY_RIGHT))){
      // X < 0?
      if(p1.get_speed_x() < 0){
	  p1.inc_speed_x(4);
      }

      // X > 319?
      else if(p1.get_speed_x() > 0){
	  p1.inc_speed_x(-4);
      }
    }

    if(!(k & (KEY_DOWN | KEY_UP ))){
      // Y < 0?
      if(p1.get_speed_y() < 0){
	  p1.inc_speed_y(4);
      }

      // Y > 239?
      else if(p1.get_speed_y() > 0){
	  p1.inc_speed_y(-4);
      }
    }

    

    p1.inc_x(time_ms);
    p1.inc_y(time_ms);
    
    if((p1.get_x() + p1.get_sprite_x()) > (X_MAX - 1))
      p1.set_x(X_MAX - p1.get_sprite_x() - 1);
    else if(p1.get_x() < 0)
      p1.set_x(0);

    if((p1.get_y() + p1.get_sprite_y()) > (Y_MAX - 1))
      p1.set_y(Y_MAX - p1.get_sprite_y() - 1);
    else if(p1.get_y() < 0)
      p1.set_y(0);
    


    // ---------------------------
    //      Enemy AI movement
    // ---------------------------
    enemies.goto_start();
    enemy_ptr = (Enemy*)enemies.get_object();
    

    while(enemy_ptr != NULL){
      // Check for enemies that have escaped
      if(enemy_ptr->get_status() == ESCAPED){
	enemies_escaped++;

	// If any enemy escapes, you loose half the score it's worth
	score_counter -= enemy_ptr->get_score_value() / 2;

	enemies.delete_obj();
      }
      // Check if enemy has been killed
      else if(enemy_ptr->get_status() == DEAD){
	// Add a small explosion for this enemy.
	// Calculate x/y so that explosion is centered over the enemy.
	int expl_x = (((int)enemy_ptr->get_sprite_x() - (int)EXPLOSION_SMALL_X)/2)
	             + enemy_ptr->get_x();
	int expl_y = (((int)enemy_ptr->get_sprite_y() - (int)EXPLOSION_SMALL_Y)/2)
	             + enemy_ptr->get_y();

        explosion_ptr = new Explosion(expl_x, expl_y, EXPLOSION_SMALL_X,
				      EXPLOSION_SMALL_Y, explosion_small,
				      EXPLOSION_SMALL_FRAMES, 1000/25);

	explosions.add_obj(explosion_ptr);

	// Increase kill counter
	enemies_killed++;

	// Increase score
	score_counter += enemy_ptr->get_score_value();

	// Remove enemy object from memory.
	enemies.delete_obj();
      }
      // Still alive, and haven't run away yet?
      // Make him do something then.
      else{
	proj_ptr = enemy_ptr->do_AI_stuff(time_ms);

	if(proj_ptr != NULL)
	  enemy_fire.add_obj(proj_ptr);
	enemies.next_obj();
      }
      enemy_ptr = (Enemy*)enemies.get_object();
    } 


    // ---------------------------
    //   Projectile AI movement
    // ---------------------------

    projectile_movement(&friendly_fire, time_ms);
    projectile_movement(&enemy_fire, time_ms);

    
    // bonus objects movement
    // check game conditions?
    // --> break;
    //     gameover = 1?



    // ---------------------------
    //       spawn enemies
    // ---------------------------
    if(lvl.enemies > 0){
      spawn_timer -= (time_ms);

      // spawn some enemies. yay!
      if(spawn_timer <= 0){
	// restart spawn timer
	rnum = rand();
	spawn_timer = ((rnum % (lvl.spawn_max - lvl.spawn_min)) 
		       + lvl.spawn_min);

	// generate random x-value:
	r_x = rand() % (X_MAX - PHOENIX_X);

	// generate random y-speed value:
	r_sy = 30 + (rand() % 80);

      
	// generate random fire-rate between 1000 and 2000 ms
	r_fr = (rand() % 1000) + 1000;

	enemy_ptr = new Enemy(r_x, -HOPELESS_Y, 0, r_sy, 0, 4, 0, 0, 100, 
			      100, 50, 50, OKAY, STRAIGHT_DOWNWARDS, r_fr,
			      100);
	enemy_ptr->set_sprite(phoenix, PHOENIX_X, PHOENIX_Y);


	enemies.add_obj(enemy_ptr);
	lvl.enemies--;
      }
    }


    // ---------------------------
    //     Collision detection
    // ---------------------------

    collision_detector(&friendly_fire, &enemies);
    collision_detector(&enemy_fire, (Spaceship *)&p1);
    collision_ship_ships((Spaceship*)&p1, &enemies);



    // ---------------------------
    //       Update animations
    // ---------------------------
    explosions.goto_start();
    explosion_ptr = (Explosion*)explosions.get_object();
    
    while(explosion_ptr != NULL){
      explosion_ptr->update_animation(time_ms);
      if(explosion_ptr->get_status() == Explosion::DONE)
	explosions.delete_obj();
      else
	explosions.next_obj();
      
      explosion_ptr = (Explosion*)explosions.get_object();
    }


    // ---------------------------
    //   CHECK GAME OBJECTIVES
    // ---------------------------
    // (currently limited to - all enemies dead?)
    if(lvl.enemies == 0 && enemies.get_size() == 0){
      *score = score_counter;
      return true;
    }
    
  }


}




static inline void draw_objects(alt_up_pixel_buffer_dev *pixel_buffer, ObjList &objects)
{
  BaseObject *obj_ptr;

  if(objects.get_size() == 0)
    return;

  objects.goto_start();
  obj_ptr = objects.get_object();

  while(obj_ptr != NULL){
    //draw_sprite(pixel_buffer, obj_ptr->get_x(), obj_ptr->get_y(), obj_ptr->get_sprite_x(),
    //		obj_ptr->get_sprite_y(), obj_ptr->get_sprite());
    obj_ptr->draw_obj(pixel_buffer);
    objects.next_obj();
    obj_ptr = objects.get_object();
  }
}


bool game_over_screen(alt_up_char_buffer_dev *char_buffer, int score,
		      unsigned int time_ms)
{
  char score_text[20];
  static int time_remaining = GAME_OVER_WAIT_TIME;

  time_remaining -= time_ms;

  // Are we done flashing the annoying game over screen?
  if(time_remaining < 0){
    // reset timer for future use.
    time_remaining = GAME_OVER_WAIT_TIME;
    return true;
  }

  // Flash GAME OVER every 500 ms.
  if((time_remaining % 1000) > 500)
    alt_up_char_buffer_string(char_buffer, "GAME OVER!", 30, 30);    
  else
    alt_up_char_buffer_string(char_buffer, "          ", 30, 30);    

  sprintf(score_text, "Your score: %d", score);
  alt_up_char_buffer_string(char_buffer, score_text, 30, 32);    
  

  return false;
}

void draw_armor_hp_score(alt_up_pixel_buffer_dev *pixel_buffer,
			 alt_up_char_buffer_dev *char_buffer, 
			 int score, Player* player)
{
  char hp_text[20];
  char armor_text[20];
  char score_text[20];
  // draw hp bar
  draw_vertical_bar(pixel_buffer, HP_ARMOR_BAR_THICKNESS, HP_BAR_Y_OFFSET,
		    HP_ARMOR_BAR_X_OFFSET, HP_ARMOR_BAR_X_OFFSET - 5 +
		    (int)(PIXELS_PER_HP * player->get_hp()),
		    HP_BAR_COLOR);

  // draw armor bar
  draw_vertical_bar(pixel_buffer, HP_ARMOR_BAR_THICKNESS, ARMOR_BAR_Y_OFFSET,
		    HP_ARMOR_BAR_X_OFFSET, HP_ARMOR_BAR_X_OFFSET - 5 + 
		    (int)(PIXELS_PER_ARMOR * player->get_armor()), 
		    ARMOR_BAR_COLOR);

  // The trailing spaces are there because the string length shrinks as
  // hp/armor is reduced they have fewer digits. Which would lead to 
  // some extra trailing digits displayed for max hp/armor.
  sprintf(hp_text,    "HP:    %d / %d   ", player->get_hp(),
	  player->get_hp_max());
  sprintf(armor_text, "Armor: %d / %d   ", player->get_armor(),
	  player->get_armor_max());
  sprintf(score_text, "Score: %d      ", score);
  alt_up_char_buffer_string(char_buffer, hp_text, 0, 1);
  alt_up_char_buffer_string(char_buffer, armor_text, 0, 2);
  alt_up_char_buffer_string(char_buffer, score_text, 0, 3);
}


void projectile_movement(ObjList* projectiles, unsigned int time_ms)
{
  Projectile* proj_ptr;

  projectiles->goto_start();
  proj_ptr = (Projectile*)projectiles->get_object();

  while(proj_ptr != NULL){
    proj_ptr->do_AI_stuff(time_ms);


    // IMPLEMENT EXPLOSIONS/ANIMATIONS!
    // Check if projectiles are out of screen etc,
    // and needs to be removed.
    if((proj_ptr->get_status() == Projectile::REMOVE_ME) ||
       (proj_ptr->get_status() == Projectile::EXPL_ANIM)){
      projectiles->delete_obj();
    }
    // The list will go to the next object 
    // if something is deleted, so we don't always 
    // have to call next_obj().
    else
      projectiles->next_obj();

    proj_ptr = (Projectile*)projectiles->get_object();
  } 


}

#include "v_bar.h"
#include <io.h>

// ASSUMES CONSECUTIVE MODE.
// NO BOUNDARY CHECK.
void draw_vertical_bar(alt_up_pixel_buffer_dev *pixel_buffer, 
		       unsigned int thickness, unsigned int y,
		       unsigned int x0, unsigned int x1, unsigned short color)
{
  register unsigned int offset;
  register unsigned int addr;
  register unsigned int y_coord;
  register unsigned int x_coord;
  register unsigned int offset_max;

  addr = pixel_buffer->back_buffer_start_address;
  
  offset = 320 * y * 2;
  offset_max = 320 * (y + thickness) * 2;

  while(offset < offset_max){
    for(x_coord = x0; x_coord < x1; x_coord++){
      IOWR_16DIRECT(addr, offset+(x_coord*2), color);
    }
    offset += 320 * 2;
  }
}

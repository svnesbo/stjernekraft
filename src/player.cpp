#include "player.h"


Player::Player(int x, int y, int speed_x, int speed_y,
	       int speed_x_max, int speed_y_max, int acc_x, int acc_y,
	       unsigned int hp, unsigned int hp_max, unsigned int armor,
	       unsigned int armor_max, _status_t status)
  : Spaceship(x, y, speed_x, speed_y, speed_x_max, speed_y_max, acc_x, acc_y,
	      hp, hp_max, armor, armor_max, status)
{

}

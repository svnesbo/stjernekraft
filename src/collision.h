#ifndef _COLLISION_H
#define _COLLISION_H

#include "list/list.h"
#include "projectile.h"
#include "spaceship.h"


void collision_detector(ObjList *fire, ObjList *ships);
void collision_detector(ObjList *fire, Spaceship *ship);
void collision_detector(Projectile *proj, Spaceship *ship);
bool collision_detector(BaseObject *obj1, BaseObject *obj2);
void collision_ship_ships(Spaceship *ship, ObjList *ships);


#endif

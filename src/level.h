#ifndef _LEVEL_H
#define _LEVEL_H


struct level_data
{
  int enemies;     // total amount of enemies for this lvl.
  int spawn_min;   // minimum spawn time between enemies (in ms)
  int spawn_max;   // maximum spawn time between enemies (in ms)
  unsigned short *background;
  unsigned int bg_y;
  unsigned int bg_scroll_speed;
};


#endif

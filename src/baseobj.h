#ifndef _BASEOBJ_H
#define _BASEOBJ_H

#include "sprite.h"

// TODO:
// (virtual?) destructors, all classes
// dynamic_cast typecasting.


class BaseObject
{
 public:
  BaseObject(int x, int y, int speed_x, int speed_y,
	     int speed_x_max, int speed_y_max, int acc_x, int acc_y);

  // Remove later, and add initializer-list to derived classes.
  BaseObject() {timer_x = 0; timer_y = 0;}
  int get_x(void) { return x; }
  int get_y(void) { return y; }
  int get_speed_x(void) { return speed_x; }
  int get_speed_y(void) { return speed_y; }
  void inc_speed_x(int speed_inc);
  void inc_speed_y(int speed_inc);
  void zero_speed_x(void) { speed_x = 0; }
  void zero_speed_y(void) { speed_y = 0; }
  //  void dec_speed_x(void);
  //  void dec_speed_y(void);

  // replace with a single update_position() function?
  void inc_x(unsigned int timer);
  void inc_y(unsigned int timer);


  void set_x(int x){ this->x = x; }
  void set_y(int y){ this->y = y; }
  void set_sprite(unsigned short *sprite, unsigned int size_x, unsigned int size_y);
  unsigned int get_sprite_x(void) { return sprite_x; }
  unsigned int get_sprite_y(void) { return sprite_y; }
  unsigned short *get_sprite(void) { return sprite; }
  void draw_obj(alt_up_pixel_buffer_dev *pixel_buffer);

 protected:
  int x,y;
  int speed_x, speed_y;

  // remove this
  int speed_x_max, speed_y_max;

  // remove this
  int acc_y, acc_x;

  unsigned short *sprite;
  unsigned int sprite_x, sprite_y;
  unsigned int timer_x;
  unsigned int timer_y;
};



#endif

#ifndef _SPACESHIP_H
#define _SPACESHIP_H

#include "baseobj.h"

enum _status_t {OKAY, DEAD, ESCAPED};


class Spaceship : public BaseObject
{
 public:
  Spaceship(int x, int y, int speed_x, int speed_y,
	    int speed_x_max, int speed_y_max, int acc_x, int acc_y, 
	    unsigned int hp, unsigned int hp_max, unsigned int armor,
	    unsigned int armor_max, _status_t status);
  unsigned int get_hp(void){ return this->hp; }
  unsigned int get_armor(void){ return this->armor; }
  unsigned int get_hp_max(void) { return this->hp_max; }
  unsigned int get_armor_max(void) { return this->armor_max; }
  _status_t get_status(void){ return this->status; }
  void damage(unsigned int amount);
  void add_hp(unsigned int amount);
  void add_armor(unsigned int amount);
 private:
  unsigned int hp;
  unsigned int hp_max;
  unsigned int armor;
  unsigned int armor_max;
 protected:
  _status_t  status;
};


#endif

#include "spaceship.h"


Spaceship::Spaceship(int x, int y, int speed_x, int speed_y,
		     int speed_x_max, int speed_y_max, int acc_x, int acc_y, 
		     unsigned int hp, unsigned int hp_max, unsigned int armor,
		     unsigned int armor_max, _status_t status)
: BaseObject(x, y, speed_x, speed_y, speed_x_max, speed_y_max, acc_x, acc_y)
{
  this->hp = hp;
  this->hp_max = hp_max;
  this->armor = armor;
  this->armor_max = armor_max;
  this->status = status;
}

void Spaceship::damage(unsigned int amount)
{
  if(armor > amount)
    armor -= amount;
  else if((hp + armor) > amount){
    amount -= armor;
    armor = 0;
    hp -= amount;
  }
  else{
    armor = 0;
    hp = 0;
    status = DEAD;
  }
}

void Spaceship::add_hp(unsigned int amount)
{
  hp += amount;
  if(hp > hp_max)
    hp = hp_max;
}
void Spaceship::add_armor(unsigned int amount)
{
  armor += amount;
  if(armor > armor_max)
    armor = armor_max;
}

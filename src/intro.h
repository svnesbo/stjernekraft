#ifndef _INTRO_H
#define _INTRO_H

#include "altera_up_avalon_pixel_buffer.h"

void intro(alt_up_pixel_buffer_dev *pixel_buffer);

#endif

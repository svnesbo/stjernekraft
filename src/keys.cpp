#include "keys.h"
#include "system.h"
//#include <io.h>
#include "altera_avalon_pio_regs.h"
#include <stdio.h>


bool keyboard_present(alt_up_ps2_dev *ps2_dev)
{
  // No keyboard?
  if(ps2_dev == NULL)
    return false;
  else if(ps2_dev->device_type != PS2_KEYBOARD)
    return false;

  // Keyboard present.
  else
    return true;
}


bool key_check(alt_up_ps2_dev *ps2_dev)
{
  if(keyboard_present(ps2_dev))
    // Check if keyboard FIFO contain any data yet.
    if(IORD_ALT_UP_PS2_PORT_RAVAIL(ps2_dev->base) > 0)
      return true;
    else
      return false;
  
  // Using impulse keys, always true.
  else
    return true;
}


unsigned int get_impulse_keys(void)
{
  unsigned int button_pio = 0;
  unsigned int sw_pio = 0;
  unsigned int key_map;

  // Read state of KEYS
  button_pio = IORD_ALTERA_AVALON_PIO_DATA(KEYS_BASE);

  // Complement all bits, as keys are active low.
  button_pio = ~button_pio;

  // Mask out unnecessary bits.
  button_pio = button_pio & 0x000F;

  // Read state of SW[7..0]
  sw_pio = IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE);

  // Mask out unnecessary bits.
  sw_pio = sw_pio & 0x00FF;

  // Join keys and switch bits together in a "key map".
  key_map = button_pio | (sw_pio << 4);


  return(key_map);
}


unsigned int get_keyboard_keys(alt_up_ps2_dev *ps2_dev, bool keys_ready)
{
  // Keyboard works a little different with make and break codes,
  // so we need to remember previous key presses.
  static unsigned int key_map = 0;
  unsigned int key_code = 0;

  int err;
  alt_u8 buf;
  char ascii;
  KB_CODE_TYPE decode_mode;
  bool break_code = false;

  if(keys_ready == false)
    return key_map;

  // Fixme:
  //
  // Could potentially start reading a multi-byte make-code before
  // everything has been received, which will cause an error.
  // Needs to be handled somehow.
  //while(key_check(ps2_dev)){
  do{
	// NOTE: altera_ups_ps2_keyboard.h needs extern "C" #ifdef clause or this won't compile!
    err = decode_scancode(ps2_dev, &decode_mode, &buf, &ascii);
    printf("decode_mode: %d, buf: '%d', err: %d, key: '%c'\n", decode_mode, buf, err, ascii);
  
    // No errors received.
    if(err == 0){
      switch(decode_mode){

      // If we get an invalid code, we'll just assume it was a break code
      // (because it probably was).
      case KB_INVALID_CODE:
      case KB_BREAK_CODE:
      case KB_LONG_BREAK_CODE:
	break_code = true;
	printf("Break code.\n");

      case KB_ASCII_MAKE_CODE:
      case KB_BINARY_MAKE_CODE:
      case KB_LONG_BINARY_MAKE_CODE:
	if(buf == KB_SPACEBAR){
		printf("Space.\n");
	  //key_code = KEY_FIRE1;
	}

	if(buf == KB_L_CTRL){
		printf("Left con-trol.\n");
		key_code = KEY_FIRE1;
	}

	else if(buf == KB_RIGHT){
	  key_code = KEY_RIGHT;
	  printf("Right.\n");
	}

	else if(buf == KB_LEFT){
	  key_code = KEY_LEFT;
	  printf("Left.\n");
	}

	else if(buf == KB_UP){
	  key_code = KEY_UP;
	  printf("Up.\n");
	}

	else if(buf == KB_DOWN){
	  key_code = KEY_DOWN;
	  printf("Down.\n");
	}

	else{
	  key_code = 0;
	  printf("Unknown key");
	}

	// If it was a break code, remove the corresponding bit from key_map.
	if(break_code == true){
	  key_code = ~key_code;
	  key_map = key_map & key_code;
	  break_code = false;
	}
	// Set the right bit if it was just make code.
	else
	  key_map = key_map | key_code;

	break;
      
      default:
	break;

      } // switch
    } // if 
  } while(IORD_ALT_UP_PS2_PORT_RAVAIL(ps2_dev->base) > 2);

  printf("key_map: %d\n", key_map);

  return key_map;
}

unsigned int get_keys(alt_up_ps2_dev *ps2_dev, bool keys_ready)
{
  if(keyboard_present(ps2_dev))
    return get_keyboard_keys(ps2_dev, keys_ready);
  
  // ignore keys_ready if we're using impulse keys.
  else
    return get_impulse_keys();
}

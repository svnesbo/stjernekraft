#ifndef _BACKGROUND_H
#define _BACKGROUND_H

#include "altera_up_avalon_pixel_buffer.h"


void draw_background(alt_up_pixel_buffer_dev *pixel_buffer, unsigned int *bg);
void draw_scrolling_background(alt_up_pixel_buffer_dev *pixel_buffer, unsigned int *bg, unsigned int bg_length, unsigned int line_offset);
void draw_scrolling_background(alt_up_pixel_buffer_dev *pixel_buffer, unsigned int *bg, unsigned int line_offset);



#endif

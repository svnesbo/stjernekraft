#include "sprite.h"
#include "system.h"
#include "altera_up_avalon_pixel_buffer.h"
#include <io.h>


// x and y coords refer to top left corner of the sprite.
void draw_sprite(alt_up_pixel_buffer_dev *pixel_buffer, int x,
		 int y, unsigned int size_x, unsigned int size_y,
		 unsigned short *data)
{
  register unsigned int addr_x;
  register unsigned int addr;
  register unsigned int addr_sprite = 0;
  register unsigned int c;
  unsigned int addr_max;
  unsigned int x_max, x_min;

  // boundary check
  //if (x >= (int)pixel_buffer->x_resolution || y >= (int)pixel_buffer->y_resolution )
  //  return;

  // If some or all of the sprite goes below the screen's max y coord.
  if((y + size_y) > 239){
    if(y > 239)
      return;
    addr_max = 239 * 320 * 2;
    addr = y * 320 * 2;
  }
  else{
    addr_max = (y + size_y) * 320 * 2;
    // If some or all of the sprite is above the screen's max y coord.
    if(y < 0){
      if((y + size_y) <= 0)
	return;
      else{
	// make an offset into the sprite equaling the y-amount outside
	// the screen
	addr_sprite = (-y) * size_x;
	// and start at zero address in the pixel buffer.
	addr = 0;
      }
    }
    // If the whole sprite is inside the screen
    else
      addr = y*320*2;
  }

  x_max = x + size_x;
  x_min = x;

  if(x_max > 319)
    x_max = 319;
  
  if(x < 0){
    if((x + size_x) > 0){
      x_min = 0;
      
      // offset sprite address by the amount we're outside the screen.
      addr_sprite += -x;
    }
    else
      return;
  }



  // ASSUMING CONSECUTIVE MODE!
  for( ; addr < addr_max; addr += 320*2){
    c = 0;
    for(addr_x = x_min; addr_x < x_max; addr_x++, c++){
      if(data[addr_sprite+c] != 0x0000)
	IOWR_16DIRECT(pixel_buffer->back_buffer_start_address, addr+(addr_x*2), data[addr_sprite+c] );
    }
    addr_sprite += size_x;
  }

  return;
}

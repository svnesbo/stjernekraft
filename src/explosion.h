#ifndef _EXPLOSION_H
#define _EXPLOSION_H

#include "baseobj.h"


// Should've been protected BaseObject really,
// but then it won't work with ObjList class :(
class Explosion : public BaseObject
{
 public:
  enum _expl_status_t {NOT_DONE_YET, DONE};

  Explosion(int x, int y, unsigned int sprite_x, unsigned int sprite_y,
	    unsigned short *animations, unsigned int n_frames, 
	    unsigned int time_per_frame);
  void update_animation(unsigned int time_elapsed);
  _expl_status_t get_status(void) { return status; }

 private:
  void set_sprite(void);
  unsigned short *animations;
  unsigned int n_frames;
  unsigned int time_per_frame;
  unsigned int time;
  _expl_status_t status;
  
};

#endif

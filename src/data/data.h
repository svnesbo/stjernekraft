#ifndef _DATA_H
#define _DATA_H


#include "../level.h"

// Backgrounds
extern unsigned short earth_bg[];
extern unsigned short char_bg[];

// Ships
extern unsigned short bc_icon[];
extern unsigned short hopeless_icon[];
extern unsigned short phoenix[];

// Projectiles
extern unsigned short laser1[];
extern unsigned short laser1_blue[];
extern unsigned short missile[];
extern unsigned short yamato[];

// Explosions
extern unsigned short explosion_small[];

// Menu data
extern unsigned short menu_bg[];
extern unsigned short menu_start_game[], menu_highscores[], menu_replay_intro[];


// In-game scrolling backgrounds
const unsigned int CHAR_BG_X = 320;
const unsigned int CHAR_BG_Y = 720;

const unsigned int EARTH_BG_X = 320;
const unsigned int EARTH_BG_Y = 720;



// Brief screen
const unsigned int BRIEF_PH_X = 320;
const unsigned int BRIEF_PH_Y = 240;



// Menu screen
const unsigned int MENU_BG_X = 320;
const unsigned int MENU_BG_Y = 240;

const unsigned int MENU_HIGHSCORES_X = 150;
const unsigned int MENU_HIGHSCORES_Y = 75;

const unsigned int MENU_INTRO_X = 150;
const unsigned int MENU_INTRO_Y = 75;

const unsigned int MENU_START_X = 150;
const unsigned int MENU_START_Y = 75;



// Projectiles
const unsigned int LASER1_X = 5;
const unsigned int LASER1_Y = 9;

const unsigned int LASER1_BLUE_X = 5;
const unsigned int LASER1_BLUE_Y = 9;

const unsigned int MISSILE_X = 5;
const unsigned int MISSILE_Y = 22;

const unsigned int YAMATO_X = 10;
const unsigned int YAMATO_Y = 25;



// Spaceships
const unsigned int BATTLECRUISER_X = 37;
const unsigned int BATTLECRUISER_Y = 36;

const unsigned int HOPELESS_X = 40;
const unsigned int HOPELESS_Y = 40;

const unsigned int PHOENIX_X = 30;
const unsigned int PHOENIX_Y = 27;




// Explosions
const unsigned int EXPLOSION_SMALL_X = 50;
const unsigned int EXPLOSION_SMALL_Y = 50;
const unsigned int EXPLOSION_SMALL_FRAMES = 15;




// Level data
const unsigned int MAX_LVL = 2;
const level_data data_levels[MAX_LVL] = 
  {{20, 1200, 3000, earth_bg, EARTH_BG_Y, 2},
   {40, 1000, 2500, char_bg, CHAR_BG_Y, 4}};


#endif

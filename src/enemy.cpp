#include "enemy.h"
#include "data/data.h"


Enemy::Enemy(int x, int y, int speed_x, int speed_y,
	     int speed_x_max, int speed_y_max, int acc_x, int acc_y,
	     unsigned int hp, unsigned int hp_max, unsigned int armor,
	     unsigned int armor_max, _status_t status, _enemy_ai_t AI_type,
	     unsigned int fire_rate, int score_value)
  : Spaceship(x, y, speed_x, speed_y, speed_x_max, speed_y_max, acc_x, acc_y,
	      hp, hp_max, armor, armor_max, status)
{
  this->AI_type = AI_type;
  this->fire_rate = fire_rate;
  this->reload_timer = 0;
  this->score_value = score_value;
}


// Implement me!
Projectile* Enemy::fire_laser(void)
{
  Projectile *proj_ptr;

  // Center the laser beam.
  unsigned int proj_x = x + ((sprite_x - LASER1_BLUE_X) / 2);
  // At the front of the ship
  unsigned int proj_y = y + sprite_y;

  proj_ptr = new Projectile(proj_x, proj_y, 0, 150, 0, 150, 0, 0,
			    Projectile::STRAIGHT_DOWNWARDS, 50,
			    Projectile::IN_TRANSIT);
  proj_ptr->set_sprite(laser1_blue, LASER1_BLUE_X, LASER1_BLUE_Y);
  return proj_ptr;
}


// void do_AI_stuff(Player the_good_guy, unsigned int time_elapsed)
Projectile* Enemy::do_AI_stuff(unsigned int time_elapsed)
{
  if(AI_type == STRAIGHT_DOWNWARDS){
    inc_y(time_elapsed);
    reload_timer += time_elapsed;

    if(reload_timer >= fire_rate){
      reload_timer -= fire_rate;
      return fire_laser();
    }
    
    if(y >= 240)
      status = ESCAPED;
  }
  // Needs access to player's baseobj..
  if(AI_type == SUICIDE_BOMBER){
  }

  return NULL;
}

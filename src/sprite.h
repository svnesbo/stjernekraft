#ifndef _SPRITE_H
#define _SPRITE_H

#include "altera_up_avalon_pixel_buffer.h"

void draw_sprite(alt_up_pixel_buffer_dev *pixel_buffer,int x, int y, 
		 unsigned int size_x, unsigned int size_y,
		 unsigned short *data);


#endif

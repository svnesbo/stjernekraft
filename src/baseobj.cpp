#include "baseobj.h"


static inline unsigned int abs(int x)
{
  if(x < 0)
    x = x * (-1);
  return x;
}


BaseObject::BaseObject(int x, int y, int speed_x,
		               int speed_y, int speed_x_max, int speed_y_max,
		               int acc_x, int acc_y)
{
  this->x = x;
  this->y = y;
  this->speed_x = speed_x;
  this->speed_y = speed_y;
  this->speed_x_max = speed_x_max;
  this->speed_y_max = speed_y_max;
  this->acc_x = acc_x;
  this->acc_y = acc_y;
  this->timer_x = 0;
  this->timer_y = 0;
}


// in pixels per second
void BaseObject::inc_speed_x(int speed_inc)
{
  speed_x += speed_inc;

  // if(speed_x > speed_x_max)
  //   speed_x = speed_x_max;
}

// in pixels per second
void BaseObject::inc_speed_y(int speed_inc)
{
  speed_y += speed_inc;

  //  if(speed_y > speed_y_max)
  //  speed_y = speed_y_max;
}

//void BaseObject::dec_speed_x(void)
//{
//  speed_x -= acc_x;
//  if(speed_x < -speed_x_max)
//    speed_x = -speed_x_max;
//}

//void BaseObject::dec_speed_y(void)
//{
//   speed_y -= acc_y;
//   if(speed_y < -speed_y_max)
//     speed_y = -speed_y_max;
// }

void BaseObject::inc_x(unsigned int timer)
{
  unsigned int x_inc;

  if(speed_x == 0)
    timer_x = 0;
  else{
    timer_x += timer;

    x_inc = timer_x / (1000 / abs(speed_x));

    timer_x = timer_x % (1000 / abs(speed_x));
  

    // CHECK BOUNDARIES??
    if(speed_x < 0)
      x -= x_inc;
    else
      x += x_inc;
  }
}

void BaseObject::inc_y(unsigned int timer)
{
  unsigned int y_inc;

  if(speed_y == 0)
    timer_y = 0;
  else{
    timer_y += timer;

    y_inc = timer_y / (1000 / abs(speed_y));

    timer_y = timer_y % (1000 / abs(speed_y));
  

    // CHECK BOUNDARIES??
    if(speed_y < 0)
      y -= y_inc;
    else
      y += y_inc;
  }
}


void BaseObject::set_sprite(unsigned short *sprite, unsigned int size_x, unsigned int size_y)
{
  this->sprite = sprite;
  this->sprite_x = size_x;
  this->sprite_y = size_y;
}

void BaseObject::draw_obj(alt_up_pixel_buffer_dev *pixel_buffer)
{
  draw_sprite(pixel_buffer, x, y, sprite_x, sprite_y, sprite);
}

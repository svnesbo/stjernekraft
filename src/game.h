#ifndef _GAME_H
#define _GAME_H

#include <alt_types.h>
#include <sys/alt_irq.h>
#include "list/list.h"
#include <altera_up_ps2_keyboard.h>
#include <altera_up_avalon_ps2_regs.h>
#include "altera_up_avalon_pixel_buffer.h"
#include "altera_up_avalon_character_buffer.h"
#include "dimensions.h"
#include "player.h"
#include "level.h"



const int GAME_OVER_WAIT_TIME = 5000;


const unsigned int PLAYER_HP_MAX = 500;
const unsigned int PLAYER_ARMOR_MAX = 500;

const unsigned int HP_ARMOR_BAR_X_OFFSET = 75;
const unsigned int HP_BAR_Y_OFFSET = 4;
const unsigned int ARMOR_BAR_Y_OFFSET = 9;
const unsigned int HP_ARMOR_BAR_THICKNESS = 3;

// A nice red color
const unsigned short HP_BAR_COLOR = 0x7800;

// A nice blue color
const unsigned short ARMOR_BAR_COLOR = 0x000F;

const float PIXELS_PER_HP = ((float)X_MAX - 1 - HP_ARMOR_BAR_X_OFFSET) / (float)PLAYER_HP_MAX;
const float PIXELS_PER_ARMOR = ((float)X_MAX - 1 - HP_ARMOR_BAR_X_OFFSET) / (float)PLAYER_ARMOR_MAX;


void game_start(alt_up_pixel_buffer_dev *pixel_buffer, 
		alt_up_char_buffer_dev *char_buffer,
		 alt_up_ps2_dev *ps2_dev);
bool game(alt_up_pixel_buffer_dev *pixel_buffer, 
	  alt_up_char_buffer_dev *char_buffer, 
	  alt_up_ps2_dev *ps2_dev, level_data lvl, int *score);
bool game_over_screen(alt_up_char_buffer_dev *char_buffer, int score, 
		      unsigned int time_ms);
void projectile_movement(ObjList* projectiles, unsigned int time_ms);
void draw_armor_hp_score(alt_up_pixel_buffer_dev *pixel_buffer, 
			 alt_up_char_buffer_dev *char_buffer, int score,
			 Player* player);


#endif

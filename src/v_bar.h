#ifndef _LINE_H
#define _LINE_H


#include "altera_up_avalon_pixel_buffer.h"


// ASSUMES CONSECUTIVE MODE.
// NO BOUNDARY CHECK.
void draw_vertical_bar(alt_up_pixel_buffer_dev *pixel_buffer, 
		       unsigned int thickness, unsigned int y,
		       unsigned int x0, unsigned int x1, unsigned short color);



#endif

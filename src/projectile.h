#ifndef _PROJECTILE_H
#define _PROJECTILE_H

#include "baseobj.h"

/* enum _proj_status_t {PROJ_IN_TRANSIT, PROJ_EXPL_ANIM, PROJ_REMOVE_ME}; */
/* enum _proj_ai_t {PROJ_STRAIGHT_UPWARDS, PROJ_STRAIGHT_DOWNWARDS,  */
/* 		 PROJ_HEATSEEKING}; */


class Projectile : public BaseObject
{
 public:
  enum _proj_status_t {IN_TRANSIT, EXPL_ANIM, REMOVE_ME};
  enum _proj_ai_t {STRAIGHT_UPWARDS, STRAIGHT_DOWNWARDS, 
		   HEATSEEKING};

  Projectile(int x, int y, int speed_x, int speed_y,
	    int speed_x_max, int speed_y_max, int acc_x, int acc_y, 
	     _proj_ai_t AI_type, unsigned int damage, _proj_status_t status);
 
  void hit(void) { status = EXPL_ANIM; }
  unsigned int get_damage(void) { return damage; }
  _proj_status_t get_status(void) { return status; }
  // Some types of ammo (such as heatseeking missiles) will probably need
  // access to the players coords etc.
  // void do_AI_stuff(Player the_good_guy, unsigned int time_elapsed);  
  void do_AI_stuff(unsigned int time_elapsed);

 private:
  _proj_ai_t AI_type;
  unsigned int damage;
 protected:
  _proj_status_t status;
};

#endif

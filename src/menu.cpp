#include "menu.h"
#include "keys.h"
#include "background.h"
#include "sprite.h"
#include "data/data.h"
#include "system.h"
#include <stdio.h>
#include <unistd.h>

unsigned int menu(alt_up_pixel_buffer_dev *pixel_buffer, alt_up_ps2_dev *ps2_dev)
{
  unsigned int menu_choice = 0;
  unsigned int key = 0;
  unsigned int key_old = 0;


  // Get previous keys
  key = get_keys(ps2_dev, false);

  
  printf("Waiting for KEY_RIGHT to be unpressed.\n");
  // The following code should probably have its own function:
  
  // Wait for KEY_RIGHT to be unpressed
  while(key & KEY_RIGHT){
    if(key_check(ps2_dev)){
      usleep(10000);
      key = get_keys(ps2_dev, true);
    }
  }


  printf("Waiting for KEY_RIGHT to be pressed.\n");
  // Wait for KEY_RIGHT to be pressed
  while(!(key & KEY_RIGHT)){
    update_menu(pixel_buffer, menu_choice);
    key_old = key;

    if(key_check(ps2_dev)){
      usleep(10000);
      key = get_keys(ps2_dev, true);
    }

    if(key != key_old){
      if(key & KEY_UP){
        if(menu_choice == 0)
	  menu_choice = MENU_MAX;
        else
	  menu_choice--;
      }
      if(key & KEY_DOWN){
        if(menu_choice == MENU_MAX)
	  menu_choice = 0;
        else
	  menu_choice++;
      }
    }
  }

  return(menu_choice);

}


void update_menu(alt_up_pixel_buffer_dev *pixel_buffer, unsigned int choice)
{ 
  // wait for buffer swap to complete
  while(alt_up_pixel_buffer_check_swap_buffers_status(pixel_buffer) != NULL)
    ;

  draw_background(pixel_buffer, (unsigned int*) &menu_bg);

  switch(choice){
  case MENU_HIGHSCORES:
    draw_sprite(pixel_buffer, MENU_X, MENU_Y, MENU_HIGHSCORES_X,
		MENU_HIGHSCORES_Y, menu_highscores);
    break;
  case MENU_INTRO:
    draw_sprite(pixel_buffer, MENU_X, MENU_Y, MENU_INTRO_X, MENU_INTRO_Y,
		menu_replay_intro);
    break;
  case MENU_START_GAME:
  default:
    draw_sprite(pixel_buffer, MENU_X, MENU_Y, MENU_START_X, MENU_START_Y,
		menu_start_game);
    break;    
  }

  alt_up_pixel_buffer_swap_buffers(pixel_buffer);
}

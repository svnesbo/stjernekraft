#include "intro.h"
#include "highscore.h"
#include "menu.h"
#include "game.h"
#include "timer.h"
#include <stdio.h>


int main(int argc, char **argv)
{
  unsigned int choice;
  alt_up_pixel_buffer_dev *pixel_buf_dev;
  alt_up_char_buffer_dev *char_buf_dev;
  alt_up_ps2_dev *ps2_dev;


  TIMER_RESET;
  TIMER_START;

  // Open the Pixel Buffer port
  pixel_buf_dev = alt_up_pixel_buffer_open_dev("/dev/pixel_buffer");
  char_buf_dev = alt_up_char_buffer_open_dev("/dev/character_buffer_0");


  // Open PS/2 device
  ps2_dev = alt_up_ps2_open_dev(PS2_0_NAME);
  alt_up_ps2_init(ps2_dev);

  if(ps2_dev->device_type == PS2_KEYBOARD){
      printf("Keyboard detected.\n");
      //ps2_dev->timeout = 1;
      set_keyboard_rate(ps2_dev, 0);
  }
  else
    printf("No keyboard found. Impulse-keys will be used instead.\n");

  if(pixel_buf_dev == NULL){
    printf("Error: could not open pixel buffer device\n");
    return -1;
  }
  else if(char_buf_dev == NULL){
    printf("Error: could not open character buffer device\n");
    return -1;
  }
  else{
    //printf("Opened pixel buffer and character buffer device\n");

    alt_up_char_buffer_init(char_buf_dev);

    intro(pixel_buf_dev);

    while(1){
      alt_up_char_buffer_clear(char_buf_dev);
      choice = menu(pixel_buf_dev, ps2_dev);

      switch(choice){
      case MENU_START_GAME:
	game_start(pixel_buf_dev, char_buf_dev, ps2_dev);
	break;
      case MENU_HIGHSCORES:
	highscores(pixel_buf_dev);
	break;
      case MENU_INTRO:
	intro(pixel_buf_dev);
	break;
      default:
	break;
      }
    }
  }
}

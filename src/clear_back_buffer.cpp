#include "clear_back_buffer.h"
#include <io.h>


// Assumes 320x240 resolution.
void clear_back_buffer(alt_up_pixel_buffer_dev *pixel_buffer)
{
	register unsigned int addr;
	register unsigned int offset = 0;
	register unsigned int limit;

	addr = pixel_buffer->back_buffer_start_address;

	limit = 320*240*2;

	while(offset < limit){
		IOWR_32DIRECT(addr, offset, 0);
		offset += 4;
	}
}

#include "projectile.h"



Projectile::Projectile(int x, int y, int speed_x, int speed_y,
		       int speed_x_max, int speed_y_max, int acc_x,
		       int acc_y, _proj_ai_t AI_type, unsigned int damage,
		       _proj_status_t status)
  : BaseObject(x, y, speed_x, speed_y, speed_x_max, speed_y_max, acc_x, acc_y)
{
  this->AI_type = AI_type;
  this->damage = damage;
  this->status = status;
}


// void do_AI_stuff(Player the_good_guy, unsigned int time_elapsed)
void Projectile::do_AI_stuff(unsigned int time_elapsed)
{
  if(AI_type == STRAIGHT_UPWARDS){
    inc_y(time_elapsed);

    if(y <= (0 - (int)sprite_y))
      status = REMOVE_ME;
  }
  else if(AI_type == STRAIGHT_DOWNWARDS){
    inc_y(time_elapsed);

    if(y >= (240 + (int)sprite_y))
      status = REMOVE_ME;
  }  
}

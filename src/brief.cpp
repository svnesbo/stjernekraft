#include "brief.h"
#include "background.h"
#include "keys.h"
#include <unistd.h>


void brief(alt_up_pixel_buffer_dev *pixel_buffer, 
	   alt_up_char_buffer_dev *char_buffer,
	   alt_up_ps2_dev *ps2_dev, unsigned int lvl)
{
  // Placeholder picture/background for briefings;
  extern unsigned short brief_ph[];
  unsigned int key = 0;
  unsigned int key_old;
  bool keys_ready = false;

  switch(lvl){
  case 0:
  case 1:
    draw_background(pixel_buffer, (unsigned int*) &brief_ph);
    alt_up_pixel_buffer_swap_buffers(pixel_buffer);
    // Load briefing screens, etc.
    break;
  default:
    break;
  }
  alt_up_char_buffer_clear(char_buffer);
  alt_up_char_buffer_string(char_buffer, "Press KEY_RIGHT to continue", 25, 55);


  // Get previous keys
  key = get_keys(ps2_dev, false);

  
  // The following code should probably have its own function:
  
  // Wait for KEY_RIGHT to be unpressed
  while(key & KEY_RIGHT){
    if(key_check(ps2_dev)){
      usleep(10000);
      key = get_keys(ps2_dev, true);
    }
  }

  // Wait for KEY_RIGHT to be pressed
  while(!(key & KEY_RIGHT)){
    if(key_check(ps2_dev)){
      usleep(10000);
      key = get_keys(ps2_dev, true);
    }
  }

  return;

  // get_keys? while loop? if there are several briefing screens for a lvl..
}
